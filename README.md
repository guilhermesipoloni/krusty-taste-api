# Krustytaste

[![CircleCI](https://circleci.com/bb/guilhermesipoloni/krusty-taste-api.svg?style=svg)](https://circleci.com/bb/guilhermesipoloni/krusty-taste-api)


Krustytaste is your online recipes book, you can save and never lost a recipe again. Moreover, you can find out new dishes and share your best recipes with other cookers if you want.  


[API Documentation](https://documenter.getpostman.com/view/122265/krustytaste-api/RW1emyNX#b8da1a98-c287-e409-fb80-b259dc70a6c8)


##Run Docker 

**Build docker image:** `docker-compose build`

**Build database:** `docker-compose up postgresql`

**Setup database:** `docker-compose run --rm web rake db:setup`

**Start application:** `docker-compose up`

**Run tests:** `docker-compose run --rm web bundle exec rspec`

##Deploy

**Automagic:** When master has a new commit or merge and all the tests is passing the deploy process will start automatically.